
```
nix-build emscripten.nix --argstr createdAt $(date --utc +%FT%TZ) 
docker load < result

docker run --rm -v `pwd`:/data -e USER_ID=`id -u` -p 3000:3000 -it juliendehos/emscripten:nix sh

docker login
docker push juliendehos/emscripten:nix
docker logout
```


