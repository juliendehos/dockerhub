{ pkgs ?  import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/17.09.tar.gz") {}
, createdAt }:

with pkgs;

let

  entrypoint = writeScript "entrypoint.sh" ''
    #!${stdenv.shell}
    set -e
    ${shadow}/bin/useradd -d /data -u $USER_ID toto
    cp -R /tmp/.emscripten_cache /data/
    chown -R toto /data
    ${gosu}/bin/gosu toto "$@"
  '';

  testcpp = writeScript "test.cpp" ''
    #include <emscripten/bind.h>
    int test() { return 0; }
    EMSCRIPTEN_BINDINGS(test) { emscripten::function("test", &test); }
  '';

in

dockerTools.buildImage {
  name = "juliendehos/emscripten";
  tag = "nix";

  runAsRoot = ''
    #!${stdenv.shell}
    ${dockerTools.shadowSetup}
    mkdir /tmp
    useradd -d /tmp toto
    chmod 777 /tmp
    cd /tmp
    cp ${testcpp} test.cpp
    ${gosu}/bin/gosu toto em++ --bind --std=c++11 -s WASM=0 -o test.js test.cpp
    ${gosu}/bin/gosu toto em++ --bind --std=c++11 -s WASM=1 -o test_wasm.js test.cpp
    userdel toto
  '';

  contents = [
    busybox
    emscripten
    gnumake
    imagemagick
    librsvg
    nodejs-8_x
    python2
    wasm
  ];

  created = createdAt;

  config = {
    Entrypoint = [ entrypoint ];
    WorkingDir = "/data";
    Volumes = { "/data" = {}; };
  };
}

