#include <emscripten/bind.h>

int test() {
    return 0;
}

EMSCRIPTEN_BINDINGS(test) {
    emscripten::function("test", &test);
}

