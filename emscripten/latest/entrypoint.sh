#!/bin/sh

adduser -D -u ${USER_ID} toto

cp -R /root/.emscripten_cache /home/toto

chown -R toto /home/toto

exec gosu toto "$@"

