
## build & push the docker image

```
docker build -t juliendehos/emscripten .
docker login
docker push juliendehos/emscripten
docker logout
```

## run (docker)

```
docker run --rm -v $(pwd):/tmp -e USER_ID=`id -u` juliendehos/emscripten npm install
```

## run (alias)

- edit `~/.bashrc` :

```
alias nenv='docker run --rm -p3000:3000 -v `pwd`:/tmp -e USER_ID=`id -u` juliendehos/emscripten'
```

- run :

```
nenv npm install
```

