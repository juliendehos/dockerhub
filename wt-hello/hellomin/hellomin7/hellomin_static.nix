{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.09.tar.gz") {} }:

with pkgs; 

let 

  _boost = boost.override { enableStatic = true; };
  _openssl = openssl.override { static = true; };
  _zlib = zlib.override { static = true; };

  _wt = (callPackage ./wt.nix {
    boost = _boost;
    openssl = _openssl;
    zlib = _zlib;
  }).overrideDerivation (attrs: { cmakeFlags = [
    attrs.cmakeFlags
    "-DSHARED_LIBS=OFF"
  ]; });

in 

  stdenv.mkDerivation {
    name = "hellomin";
    src = ./.;
    nativeBuildInputs = [ _wt _boost _openssl _zlib.static glibc.static ];
    buildPhase = "g++ -O2 -o hellomin hellomin.cpp -pthread -static -lwthttp -lwt -lboost_system-mt -lboost_program_options-mt -lboost_filesystem-mt -lboost_thread-mt -lssl -lcrypto -ldl -lz";
    installPhase = ''
      mkdir -p $out/bin
      cp hellomin $out/bin/
    '';
  }

