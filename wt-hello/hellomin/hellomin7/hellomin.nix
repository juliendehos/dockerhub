{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.09.tar.gz") {} }:

with pkgs; 

let 

  _wt = callPackage ./wt.nix {};

in 

  stdenv.mkDerivation {
    name = "hellomin";
    src = ./.;
    nativeBuildInputs = [ _wt ];
    buildPhase = "g++ -O2 -o hellomin hellomin.cpp -lwt -lwthttp";
    installPhase = ''
      mkdir -p $out/bin
      cp hellomin $out/bin/
    '';
  }

