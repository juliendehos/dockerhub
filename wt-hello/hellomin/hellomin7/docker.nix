{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.09.tar.gz") {} }:

let

  myapp = pkgs.callPackage ./hellomin.nix {};

in

  pkgs.dockerTools.buildImage {
    name = "hellomin";
    tag = "7";
    config.Cmd = [ "" ];
    config.Entrypoint = [ "${myapp}/bin/hellomin" ];
  }

