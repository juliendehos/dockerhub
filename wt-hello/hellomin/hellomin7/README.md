# hellomin7

## build & run locally

```
nix-build hellomin.nix
nix-build hellomin_static.nix

./result/bin/hellomin
```

## build & run a docker image

```
nix-build --cores 12 docker.nix && docker load < result
nix-build --cores 12 docker_static.nix && docker load < result

docker run --rm -it -e PORT=3000 -p 3000:3000 hellomin:7
```

image size: TODO
 

## deploy on heroku

```
heroku container:login
heroku create hellomin
docker tag hellomin:7 registry.heroku.com/hellomin/web
docker push registry.heroku.com/hellomin/web
heroku container:release web --app hellomin
```

