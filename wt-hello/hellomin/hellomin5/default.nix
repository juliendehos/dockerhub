{ pkgs ? import <nixpkgs> {} }: 

let
  mywt = pkgs.callPackage ./wt.nix {};
in

pkgs.stdenv.mkDerivation {
  name = "hellomin";
  src = ./.;
  buildInputs = [ mywt ];
  buildPhase = "g++ -O2 -o hellomin hellomin.cpp -lwt -lwthttp";
  installPhase = ''
    mkdir -p $out/bin
    cp hellomin $out/bin/
  '';
}

