{ pkgs ? import <nixpkgs> {} }: 

let
  myboost = pkgs.boost.override {
    enableStatic = true;
    enableShared = false;
  };

  myopenssl = pkgs.openssl.override {
    static = true;
    #withCryptodev = true; 
    #cryptodevHeaders = false;
  };

  mywt = pkgs.callPackage ./wt.nix {};
in

pkgs.stdenv.mkDerivation {
  name = "hellomin";
  src = ./.;

  nativeBuildInputs = [
    mywt 
    myboost
    myopenssl
    pkgs.glibc.static
    pkgs.zlib.static
  ];

  #buildPhase = "g++ -static -O2 -o hellomin hellomin.cpp -lwt -lboost_system -lboost_thread -lboost_filesystem -lboost_program_options -pthread -lssl -lcrypto -lz";
  buildPhase = "touch hellomin";

  installPhase = ''
    mkdir -p $out/bin
    cp hellomin $out/bin/
  '';
}

