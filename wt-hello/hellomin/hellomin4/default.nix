{ pkgs ? import <nixpkgs> {} }: 

pkgs.stdenv.mkDerivation {
  name = "hellomin";
  src = ./.;
  buildInputs = [ pkgs.wt ];
  buildPhase = "g++ -O2 -o hellomin hellomin.cpp -lwthttp -lwt";
  installPhase = ''
    mkdir -p $out/bin
    cp hellomin $out/bin/
  '';
}

