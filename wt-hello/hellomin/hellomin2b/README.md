# hellomin2b

## build & run a docker image

```
docker build -t hellomin:2b .
docker run --rm -it -e PORT=3000 -p 3000:3000 hellomin:2b
```

image size: 83MB
 

## deploy on heroku

```
heroku container:login
heroku create hellomin
heroku container:push web --app hellomin
heroku container:release web --app hellomin
```

