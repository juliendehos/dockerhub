#include <Wt/WApplication.h>
#include <Wt/WBreak.h>
#include <Wt/WContainerWidget.h>
#include <Wt/WLineEdit.h>
#include <Wt/WText.h>

#include <cstdlib>

using namespace std;
using namespace Wt;

struct App : WApplication {
  App(const WEnvironment& env) : WApplication(env) {
    auto myEdit = root()->addWidget(make_unique<WLineEdit>());
    root()->addWidget(make_unique<WBreak>());
    auto myText = root()->addWidget(make_unique<WText>());
    auto editFunc = [=]{ myText->setText(myEdit->text()); };
    myEdit->textInput().connect(editFunc);
  }
};

int main() {
  int argc = 7;
  char * port = getenv("PORT");
  const char * argv[] = { "/hellomin",
    "--docroot", ".", "--http-address", "0.0.0.0", "--http-port", port, 0};
  auto mkApp = [](const WEnvironment& env) { return make_unique<App>(env); };
  return WRun(argc, (char **)argv, mkApp);
}

