# hellomin1

## build & run a docker image

```
docker build -t hellomin:1 .
docker run --rm -it -e PORT=3000 -p 3000:3000 hellomin:1
```

image size: 1.64GB
 

## deploy on heroku

```
heroku container:login
heroku create hellomin
heroku container:push web --app hellomin
heroku container:release web --app hellomin
```

