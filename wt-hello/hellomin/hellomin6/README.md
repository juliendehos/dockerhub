# hellomin6

## build & run locally

```
nix-build
./result/bin/hellomin --docroot . --http-address 0.0.0.0 --http-port 3000
```

## build & run a docker image

```
nix-build --cores 12 docker.nix && docker load < result
docker run --rm -it -e PORT=3000 -p 3000:3000 hellomin:6
```

image size: 105MB
 

## deploy on heroku

```
heroku container:login
heroku create hellomin
docker tag hellomin:6 registry.heroku.com/hellomin/web
docker push registry.heroku.com/hellomin/web
heroku container:release web --app hellomin
```

