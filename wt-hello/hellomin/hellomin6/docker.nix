{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.09.tar.gz") {} }:

let

  mywt = pkgs.callPackage ./wt.nix {};

  myapp = pkgs.callPackage ./default.nix { wt = mywt; };

  entrypoint = pkgs.writeScript "entrypoint.sh" ''
    #!${pkgs.stdenv.shell}
    $@ --docroot . --http-address 0.0.0.0 --http-port $PORT
  '';

in

  pkgs.dockerTools.buildImage {
    name = "hellomin";
    tag = "6";
    config = {
      Entrypoint = [ entrypoint ];
      Cmd = [ "${myapp}/bin/hellomin" ];
    };
  }

