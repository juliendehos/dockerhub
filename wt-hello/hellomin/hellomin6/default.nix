{ pkgs ? import <nixpkgs> {}, wt ? pkgs.wt }: 

pkgs.stdenv.mkDerivation {
  name = "hellomin";
  src = ./.;
  buildInputs = [ wt ];
  buildPhase = "g++ -O2 -o hellomin hellomin.cpp -lwt -lwthttp";
  installPhase = ''
    mkdir -p $out/bin
    cp hellomin $out/bin/
  '';
}

