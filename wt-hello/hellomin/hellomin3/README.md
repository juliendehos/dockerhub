# hellomin3

## build & run locally

```
nix-build
./result/bin/hellomin --docroot . --http-address 0.0.0.0 --http-port 3000
```

## build & run a docker image

```
docker build -t hellomin:3 .
docker run --rm -it -e PORT=3000 -p 3000:3000 hellomin:3
```

image size: 1.17GB
 

## deploy on heroku

```
heroku container:login
heroku create hellomin
heroku container:push web --app hellomin
heroku container:release web --app hellomin
```

