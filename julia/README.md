
## build & push the docker image

```
docker build -t juliendehos/julia .
docker login
docker push juliendehos/julia
docker logout
```

## run (docker)

```
docker run --rm -v $(pwd):/src -e USER_ID=`id -u` -ti juliendehos/julia 
```

