with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/17.09.tar.gz") {};

let

  _miso = haskell.packages.ghcjs.callCabal2nix "miso" (fetchFromGitHub {
    rev = "0.21.1.0";
    sha256 = "19x9ym4399i6ygs0hs9clgrvni0vijfg4ff3jfxgfqgjihbn0w4r";
    owner = "haskell-miso";
    repo = "miso";
  }) {};

in

  stdenv.mkDerivation {
    name = "mymiso";
    buildInputs = [
      _miso
      closurecompiler
    ];
  }

