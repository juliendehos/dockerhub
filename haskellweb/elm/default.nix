with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") {};

stdenv.mkDerivation {
  name = "myelm";
  buildInputs = [
    closurecompiler
    elmPackages.elm-make
  ];
}

