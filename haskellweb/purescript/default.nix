with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/10b979ff213d7e63a6e072d0433a21687e311483.tar.gz") {};

stdenv.mkDerivation {
  name = "mypurs";
  buildInputs = [
    closurecompiler
    cacert
    git
    nodejs-8_x
    nodePackages.bower
    nodePackages.pulp
    purescript
  ];
}

