#with import <nixpkgs> {};
with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") {};

# import ((import <nixpkgs> {}).fetchFromGitHub (builtins.fromJSON (builtins.readFile ./github.json)))
# {
#   "owner": "obsidiansystems",
#   "repo": "nixpkgs",
#   "rev": "456be5f5f9ff24cf5a1daffafe3741fdbddc876b",
#   "sha256": "0x4p58czs62fps9rxqqrg1pv6mjlhzl3vik9a99sp0b5pxr1s1p3"
# }

let 

   _rp_ref = "ea3c9a1536a987916502701fb6d319a880fdec96";  # 2018-04-17
  _rp = import (fetchTarball "https://github.com/reflex-frp/reflex-platform/archive/${_rp_ref}.tar.gz") {};

  _project = _rp.project ({ pkgs, ... }: {
    packages = {
      helloreflex = ./helloreflex;
    };
    shells = {
      ghcjs = ["helloreflex"];
    };
  });

in

  stdenv.mkDerivation rec {
    name = "helloreflex";
    src = ./.;
    buildInputs = [
      _project 
      closurecompiler
    ];
    appdir = "${_project.ghcjs.helloreflex}/bin/helloreflex.jsexe";
    buildPhase = ''
      ${closurecompiler}/bin/closure-compiler ${appdir}/all.js > all.js
    '';
    installPhase = ''
      mkdir -p $out
      cp index.html all.js $out/
    '';
  }

